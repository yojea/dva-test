import React, {Component, PropTypes} from 'react';

import UserList from '../components/Users/UserList'
import UserSearch from '../components/Users/UserSearch'
import UserModal from '../components/Users/UserModal'

import styles from './Users.css'

/**
 * User Container Component
 */
function Users() {
    const userSearchProps = {}
    const userListProps = {
        total: 3,
        current: 1,
        loading: false,
        dataSource: [
            {
                name: '张三',
                age: 12,
                address: '北京'
            },
            {
                name: '张三',
                age: 12,
                address: '北京'
            },
            {
                name: '张三',
                age: 12,
                address: '北京'
            }
        ]
    }
    const userModalProps = {}

    return (
        <div className={styles.normal}>
            {/*用户筛选搜索框*/}
            <UserSearch {...userSearchProps} />
            {/*用户信息展示列表*/}
            <UserList {...userListProps} />
            {/*添加用户 & 修改用户弹出浮层*/}
            <UserModal {...userModalProps}/>
        </div>
    );
      
}

Users.propTypes = {
};

export default Users;
